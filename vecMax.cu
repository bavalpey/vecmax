#include <iostream>
#include <math.h>
#include <limits>
#include <random>
#include <assert.h>
#include <helper_cuda.h>

#   define CUDA_SAFE_CALL_NO_SYNC( call) {                              \
    cudaError err = call;                                               \
    if( cudaSuccess != err) {                                           \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s. \n",  \
                __FILE__, __LINE__, cudaGetErrorString( err) );         \
        exit(EXIT_FAILURE);                                             \
    } }

#   define CUDA_SAFE_CALL( call)    CUDA_SAFE_CALL_NO_SYNC(call);
#define BLOCK_WIDTH 256
#define FLOAT_MIN (int) std::numeric_limits<int>::min();
__global__
void max(const int *a, int *i, int N)
{
    __shared__ int temp[BLOCK_WIDTH]; 
    int indx = blockIdx.x*blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;
    int thisMax = -1;
    for (int j = indx; j < N; j+=stride) {
        if(thisMax < a[j])
            thisMax = a[j];
    }
    temp[threadIdx.x] = thisMax;
    __syncthreads();
    
    if(threadIdx.x == 0){
        for (int j = 0; j < blockDim.x; ++j) {
            if(thisMax < temp[j]){
                thisMax = temp[j];
            }
        }
        // printf("%g\n", thisMax);
        i[blockIdx.x] = thisMax;
    }

}
__global__
void aMax(const int *a, int *i, int N)
{
    __shared__ int localMax[1];
    int indx = blockIdx.x*blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;
    int thisMax = -1;
    for (int j = indx; j < N; j+= stride) {
        if (thisMax < a[j])
            thisMax = a[j];
    }
    atomicMax(&localMax[0], thisMax);
    __syncthreads();

    if (threadIdx.x == 0)
        atomicMax(&i[0], localMax[0]);
}

int main(void){
    cudaEvent_t start, stop, start2, stop2;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    int N = 1<<20;
    size_t size = N*sizeof(int);
    int *d_x;
    int *x = new int[N];

    for (int i = 0; i < N; ++i)
    {
        x[i] = i;
        // x[i] = ((int) rand() / (RAND_MAX));
    }

    std::cout << "The first element is: " << x[0] << std::endl;
    int *ans_d;
    int *ans = new int[1];
    cudaMalloc(&d_x, size);
    CUDA_SAFE_CALL(cudaMemcpy(d_x, x, N*sizeof(int), cudaMemcpyHostToDevice));
    int num_blocks = (N + BLOCK_WIDTH - 1) / BLOCK_WIDTH;
    cudaMalloc(&ans_d, sizeof(int)*num_blocks);
    // can only synchronize blocks by calling the kernel again. Continue calling kernels until size of answer is 1
    bool even = true;
    cudaEventRecord(start);
    while(N != 1){
        std::cout << "N is: " << N << " Block size is: " << num_blocks << std::endl;
        if(even)
            max<<<num_blocks, BLOCK_WIDTH>>>(d_x, ans_d, N);
        else
            max<<<num_blocks, BLOCK_WIDTH>>>(ans_d, d_x, N);
        N = num_blocks;
        if(N <= BLOCK_WIDTH)
            num_blocks = 1;
        else
            num_blocks /= 2;
        even = ! even;
    }
    cudaEventRecord(stop);
    if(even) {
        CUDA_SAFE_CALL(cudaMemcpy(ans, ans_d, sizeof(int), cudaMemcpyDeviceToHost));
    }
    else {
        CUDA_SAFE_CALL(cudaMemcpy(ans, d_x, sizeof(int), cudaMemcpyDeviceToHost));
    }
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout << "Computed ans: " << ans[0] << ". Elapsed time: " << milliseconds << std::endl;

    // Now, let's see the performance of atomic max.

    int *ans_d2, *d_x2;
    int *ans2 = new int[1];
    cudaMalloc(&d_x2, size);
    CUDA_SAFE_CALL(cudaMemcpy(d_x2, x, N*sizeof(int), cudaMemcpyHostToDevice));
    num_blocks = (N + BLOCK_WIDTH - 1) / BLOCK_WIDTH;
    cudaMalloc(&ans_d2, sizeof(int));
    cudaEventCreate(&start2);
    cudaEventCreate(&stop2);
    cudaEventRecord(start2);
    aMax<<<num_blocks, BLOCK_WIDTH>>>(d_x2, ans_d2, 1<<20);
    cudaDeviceSynchronize();
    cudaEventRecord(stop2);
    CUDA_SAFE_CALL(cudaMemcpy(ans2, ans_d2, sizeof(int), cudaMemcpyDeviceToHost));
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&milliseconds, start2, stop2);
    std::cout << "Computed ans: " << ans2[0] << ". Elapsed time: " << milliseconds << std::endl;
    cudaFree(d_x);
    cudaFree(d_x2);
    cudaFree(ans_d2);
    cudaFree(ans_d);
    free(x);
    return 0;
}
